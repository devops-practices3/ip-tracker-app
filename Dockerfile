FROM golang:1.21-alpine3.18 AS builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN go build -o ip-tracker

FROM golang:1.21-alpine3.18
WORKDIR /app
COPY --chown=nobody:nobody --from=builder /app/ip-tracker .
USER nobody
ENTRYPOINT ["./ip-tracker"]